# Package

version       = "0.1.0"
author        = "Rokas Urbelis"
description   = "Telegram bot to make sticker quotes"
license       = "GPL-3.0-only"
srcDir        = "src"
bin           = @["sticky_bot"]


# Dependencies

requires "nim >= 1.4.6"
requires "telebot >= 1.0.11"
requires "pixie >= 3.0.3"