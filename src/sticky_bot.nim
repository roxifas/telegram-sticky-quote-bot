import telebot, asyncdispatch, logging, options, pixie, strformat, httpclient, os

var L = newConsoleLogger(fmtStr="$levelname, [$time] ")
addHandler(L)

const API_KEY = slurp("../bot-key.txt")

const telegramStickerConstraint = 512
const stickerMessageBoxPadding = 18
const stickerUserPhotoSize = 100
const stickerUserPhotoMessageBoxSpacing = 32
const stickerTextSizeBounds = vec2(
      telegramStickerConstraint - stickerMessageBoxPadding * 2 - stickerUserPhotoMessageBoxSpacing - stickerUserPhotoSize,
      telegramStickerConstraint - stickerMessageBoxPadding * 2
    )
const stickerBackgroundColor = parseHtmlColor("#130f1c").rgba
const stickerForegroundColor = parseHtmlColor("#e8eaea")


proc newFont(typeface: Typeface, size: float32, color: Color): Font =
  result = newFont(typeface)
  result.size = size
  result.paint.color = color

let typeface = readTypeface("data/Ubuntu-Regular_1.ttf")
let font = newFont(typeface, 24, stickerForegroundColor)


proc measureText(text: string): Vec2 =
  let arrangement = font.typeset(
    text,
    stickerTextSizeBounds
  )
  return arrangement.computeBounds()

proc downloadTelegramFile(telegramPath: string): Future[string] {.async.} = 
  let client = newAsyncHttpClient()
  createDir("downloads") #Ensures this dir exists, otherwise below might error
  await client.downloadFile(fmt"https://api.telegram.org/file/bot{API_KEY}/{telegramPath}", "downloads/test.png")
  return "downloads/test.png"

proc drawUserImage(image: Image, pathToUserImage: string, verticalPosition: float) =
  let path = newPath()
  path.circle(stickerUserPhotoSize / 2, verticalPosition + stickerUserPhotoSize / 2, stickerUserPhotoSize / 2)
  let userImage = readImage(pathToUserImage).resize(stickerUserPhotoSize, stickerUserPhotoSize)
  let paint = newPaint(pkImage)
  paint.image = userImage
  paint.imageMat = translate(vec2(0, verticalPosition))
  image.fillPath(path, paint)

proc drawMessageBox(image: Image, textSize: Vec2): float =
  let ctx = newContext(image)
  ctx.fillStyle = stickerBackgroundColor
  #TODO: no this does not make the box vertically centered lol
  let boxVerticalPosition = (telegramStickerConstraint - textSize.y - stickerMessageBoxPadding) / 2
  let
    pos = vec2(stickerUserPhotoSize + stickerUserPhotoMessageBoxSpacing, boxVerticalPosition)
    wh = vec2(textSize.x + stickerMessageBoxPadding * 2, textSize.y + stickerMessageBoxPadding * 2)
    r = 15.0
  ctx.fillRoundedRect(rect(pos, wh), r)
  return boxVerticalPosition

proc makeSticker(text: string, pathToUserImage: string): Future[string] {.async.} =
  let image = newImage(512, 512)
  let textSize = measureText(text)
  let boxVerticalPosition = drawMessageBox(image, textSize)
  image.fillText(
    font,
    text,
    translate(vec2(stickerUserPhotoSize + stickerUserPhotoMessageBoxSpacing + stickerMessageBoxPadding, boxVerticalPosition + stickerMessageBoxPadding)),
    stickerTextSizeBounds
  )

  drawUserImage(image, pathToUserImage, boxVerticalPosition)
  image.writeFile("text.png")
  return "text.png"

proc updateHandler(b: Telebot, u: Update): Future[bool] {.async, gcsafe.} =
  if not u.message.isSome:
    # return true will make bot stop process other callbacks
    return true
  var response = u.message.get
  if response.text.isSome:
    let userPhotos = await b.getUserProfilePhotos(response.fromUser.get.id) #TODO: handle option safely
    #TODO: For now this just assumes we'll have a profile image, which is not really true
    #let photoFile = await b.getFile(userPhotos.photos[0][0].fileId)
    #let photoFileTelegramPath = photoFile.filePath.get
    #let photoFileLocalPath = await downloadTelegramFile(photoFileTelegramPath)
    let photoFileLocalPath = "downloads/test.png"
    let stickerImagePath = await makeSticker(response.text.get, photoFileLocalPath)
    discard await b.sendSticker(response.chat.id, fmt"file://{stickerImagePath}", replyToMessageId = response.messageId)

let bot = newTeleBot(API_KEY)
bot.onUpdate(updateHandler)
bot.poll(timeout=300)